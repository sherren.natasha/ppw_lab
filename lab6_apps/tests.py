from django.test import *
from django.urls import resolve
from .views import index
from .models import Status

class MyFirstTests(TestCase):
    
    def test_hello(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_hello_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_template_hello(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'hello.html')

    def test_landing_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('Halo, apa kabar?',html_response)
    def test_create_object_model (self):
        status_message = Status.objects.create(mystatus = 'ok')
        counting_object_status = Status.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_profile_page_(self):
        response = Client().get('/')
        html_response = response.content.decode('utf-8')
        self.assertIn('PROFIL SAYA',html_response)


