from django.apps import AppConfig


class Lab6AppsConfig(AppConfig):
    name = 'lab6_apps'
